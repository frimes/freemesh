#!/bin/sh

#simple script to flash the LED so we know the flashing of the new firmware is complete

#for now.  The multi-colored lights on the latest look weird.
echo "none" > /sys/class/leds/mt76-phy0/trigger;
echo "none" > /sys/class/leds/mt76-phy1/trigger;

echo "0" > /sys/class/leds/mt76-phy0/brightness;
echo "0" > /sys/class/leds/mt76-phy1/brightness;

led_state=0;

while true
do
	if [ "$led_state" == 0 ]; then
		led_state=1;
	else
		led_state=0;
	fi

	echo "$led_state" > /sys/class/leds/mt76-phy0/brightness;
	echo "$led_state" > /sys/class/leds/mt76-phy1/brightness;	

	sleep 1;
done
